# Revision history for pretty-loc

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.

## 0.1.0.1 -- 2019-06-19

* Added the git repository path in the .cabal file
